/* To je neka zelo osnova razlicica programa, ki ga bom kasneje (bog si ga vedi kdaj)
 * v celoti pretvoril v zbirnik - do takrat bom uporabljal inline assembly s C jem, ter
 * prisilil prevajalnik, da mi ta izpljune zbirnik ven
 
   da prisilim prevajalnik, da mi vrze ven assembly, uporabim
   bcc -v -C-d -S -Md conway.c
   
   Na Arch Linuxu lahko vsa potrebna orodja za prevajanje tega pridobis z:
   pacman -S dev86
   
   =============================================================================
   STVARI NA KATERE MORAS NUJNO PAZITI KO SI V INLINE ASSEMBLY NACINU:
   
   karkoli nima # na zacetku je NASLOV (ne glede na to ali je hex ali dec)
   torej IMMEDIATE vrednosti imajo OBVEZNO # na zacetku
   
   Ker bcc inline assembly direktno posreduje as86 assemblerju, veljajo zanj vse
   njegove cake - glej man as86!
 */

/* staticne spremenljivke/podatki */
char sporocilo[] = "Dobrodosel na planetu zemlja!";

/* funkcija za izpis niza */
void izp_niz(niz)
/* po pradavnem K&R C imamo mi v oklepajih le seznam argumentov, nakar definiramo
   se njihov tip pred blokom - in seveda, ne pozabi steti, ko se gremo */
/* BCC uporablja base pointer za naslavljanje na okvir sklada
   Sestava okvirja:
      - argumenti funkcije (pozitiven indeks), nalozi klicatelj
         - kazalci v 8086 zavzamejo 16 bitov (2B)
         - int tip prav tako zavzame 16 bitov
      - prvotni kazalec sklada (indeks 0)
      - prvotne vrednosti vseh registrov (vsak 16 bit) (negativen indeks),
        nalozi jih funkcija sama predno se zacne izvajanje njene vsebine
      - lokalne spremenljivke
   Ne pozabi da sklad narasca navzdol!*/
char *niz; /* 16 biten pointer, 2B */
{
   int i=0; /* lokalna spremenljivka, se nalozi na konec stack frame-a = -6[bp] */
   char tznak; /* -8[bp]  */
   while((tznak = niz[i])!=0)
   {
#asm
      /* shranjevanje na sklad naredimo zato, da ne spremenimo vrednost ax na
       * nekaj, kar prevajalnik morda ne pricakuje */
      push ax
      push bx
      /* klicemo 0F, da pridobimo trenuten video nacin, med njimi BH za trenuten
         page number */
      mov ah,  #0x0F
      int      #0x10
      
      /* nastavimo znak v AL ter klicemo 0E za teletype output */
      mov ah,  #0x0E
      mov al, -7[bp]
      int      #0x10
      
      pop bx
      pop ax /* povrnemo stanje resitrov nazaj na prevajalnikovo okolje */
#endasm
      i++;
   }
}

int main()
{
/* inicializacija graficnega nacina */
#asm
   push ax
   mov ah,     #0x00
   mov al,     #0x03
   int         #0x10
   pop ax
#endasm
   izp_niz(sporocilo);
/* vrnemo se v DOS */
#asm
   mov ah,     #0x4C
   mov al,     #0x00
   int         #0x21
#endasm
}

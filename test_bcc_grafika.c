int main(){
    #asm
    // ce ti pri int10 pustis ah (tj. visji del ax registra) prazen, bo
    // al definiral graficni nacin
    mov ax, #0x13
    int #0x10

    // poskusajmo priti do graficnega pomnilnika
    mov ax, #0xA000
    mov ds, ax
    mov bx, #0x0
zanka:
    mov [bx], #0xF
    add bx, #1
    cmp bx, #0xFA00
    jb zanka

noop:
    jmp noop

    #endasm
    return 0;
}


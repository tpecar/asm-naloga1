/* To je neka zelo osnova razlicica programa, ki ga bom kasneje (bog si ga vedi kdaj)
 * v celoti pretvoril v zbirnik - do takrat bom uporabljal inline assembly s C jem, ter
 * prisilil prevajalnik, da mi ta izpljune zbirnik ven
 
   da prisilim prevajalnik, da mi vrze ven assembly, uporabim
   bcc -v -C-d -S -Md conway.c
   
   Na Arch Linuxu lahko vsa potrebna orodja za prevajanje tega pridobis z:
   pacman -S dev86
   
   =============================================================================
   STVARI NA KATERE MORAS NUJNO PAZITI KO SI V INLINE ASSEMBLY NACINU:
   
   karkoli nima # na zacetku je NASLOV (ne glede na to ali je hex ali dec)
   torej IMMEDIATE vrednosti imajo OBVEZNO # na zacetku
   
   Ker bcc inline assembly direktno posreduje as86 assemblerju, veljajo zanj vse
   njegove cake - glej man as86!
   
   paziti moras da so kazalci 16-bitni, in da ce prirejas vec kot 16-bit vrednost, se
   ta preprosto odreze.
   
   Ce ti dejansko zelis naslavljati 20-bitno, moras to narediti v zbirniku, saj
   sprememba ds registra pokvari shranjevanje globalnih spremenljivk, tabel ter
   kazalcev v BCCju, ki se vsi povecini zanasajo na bx register.
      So primeri, ko se se vedno da naslavljati posamezne spremenljivke, vsaj
      lokalne ter argumenti so predvidoma varni (ker uporabljajo bp, ki je vezan na ss),
      ceprav YMMV
      
   V primeru da ti as86 vrne skrajno nerazumljivi
   'non-imported expression required', to v veliki vecini primerov pomeni, da
   si se preprosto zatipkal v imenu labele - ce on vidi labelo, katere ciljini naslov v
   sami datoteki ni definiran, bo pogledal ce so kaksne ekstrno (tj, globalno,
   za vse datoteke, ki se trenutno povezujejo) dostopne pod tem imenom, a to
   prepreci (mogoce moras to posebej napovedati?).
   
   ZA LAZJE DEBUGGANJE lahko v kodi klicemo klicemo single step interrupt,
      int 0x1
   (ki je zgleda da no-op) da ga lahko ujamemo z dosbox debuggerjem.
   Da se ustavimo na njem, uporabimo BPINT 1
   To bi lahko mocno pospesilo cikel prevajanja ter razhroscevanja, saj ti vsaj
   ni treba iskati tistega specificnega ukaza po pomnilniku vsakic ko ponovno
   prevedes program.
   =============================================================================
   
   Kaj in kako naj bi ta program pocel:
      gre se za Conway's game of life, pri cemer izvajam celotne operacije na
      pomnilniku graficne kartice v 13h graficnem nacinu.
      Pri simulaciji bo potrebno preklapljati med dvemi stanji: trenutnim ter
      prejsnjim ter iz prejsnjega zgenerirati trenutno stanje ter v naslednjem
      ciklu trenutnega nastaviti na prejsnjega (in celoten cikel se ponovi).
      
      V idealnem svetu bi mi za to lahko uporabljali 2 tabeli, eno za prejsnje in
      drugo za trenutno stanje, vendar ker imamo omejen pomnilniski prostor in
      ker so dostopi do pomnilnika izrazito pocasni (se posebno do video pomnilnika),
      bom nekako poskusal "pospesiti" ter stisniti zadevo tako, da bom obe stanji
      stisnil kar v video pomnilnik.
      
      Mi namrec za stanje posamezne celice potrebujemo le en bit (ali je ziva ali pa ne),
      torej bom uporabil 2 razlicna bita v bajtu za shranjevanje le-teh, ob tem pa bom
      nastavil palete tako, da bom ob preklopu palet prikazal novo stanje ter skril prejsnjega.
      
      Ko tvorimo novo stanje, imamo lahko po koncu izracuna:
      I. CIKEL
      
      trenutni|prejsnji
              |
      0000 000|0 - 0x0 (nezivo) - skrito
      0000 001|0 - 0x2 (nezivo v prejsnjem, a zivo v trenutnem) - skrito do preklopa
      0000 000|1 - 0x1 (zivo v prejsnjem, a nezivo v trenutnem) - vidno do preklopa
      0000 001|1 - 0x3  (zivo v prejsnjem ter v trenutnem) - vidno pred in po preklopu
      
      Ko generiramo novo stanje, gledamo za prejsnjega le spodnji bit in na podlagi
      njegove okolice gledamo ali bo ziv ali mrtev v trenutnem - rezultat shranimo
      kot (bajt prejsnjega) | (trenutnio stanje<<1 )
      
      Ker je zdaj trenutno stanje na visjem bitu in da izognemo shiftanju, bomo pri
      zamenjavi stanj tudi zamenjali vlogo bitov
      
      II. CIKEL
      
      prejsnji|trenutni
              |
      0000 000|0 - 0x0 (nezivo) - skrito
      0000 001|0 - 0x2 (zivo v prejsnjem, a nezivo v trenutnem) - vidno do preklopa
      0000 000|1 - 0x1 (nezivo v prejsnjem, a zivo v trenutnem) - skrito do preklopa
      0000 001|1 - 0x3 (zivo v prejsnjem ter v trenutnem) - vidno pred in po preklopu
      
      To si v osnovi lahko naredimo kar z masko 0x1, ki jo v vsakem ciklu negiramo, oz
      
      maska prejsnjega cikla - preberemo prejsne bite, na njihovi podlagi dolocimo
         vrednost trenutnega bita
      maska = ! maska
         shranimo trenutni bit
      ker moramo se obdelati preostalo sliko, naredimo spet maska = !maska
      da preklopimo v prejsne stanje.
      
      Nato pa na koncu naredimo se en dodaten maska + !maska da preklopimo v drugi
      cikel. Ter preklopimo paleto - ce gledamo zgornje tabele, lahko to naredimo
      kar tako da za barvo 0x1 ter 0x2 shranimo masko - za 0x1 predno spremenimo,
      za 0x2 potem ko masko spremenimo.
      
      Pri gledanju po prostoru bomo poskusali implementirati toroidni pomnilnik, kar
      pomeni, da je ob robovih potrebno gledati drugo stran.
      Mi vemo, da je nas prostor 320x200, pri cemer ga naslavljamo linearno
      (kot enodimenzionalno tabelo), kar pomeni sledece
                                 
              (x-320)            
                ^                
                |                
       (x-1) <--x--> (x+1)       
                |                
                v                
              (x+320)            
   
      Pri cemer za prvo in zadnjo vrstico testiramo naslov za negativno, naslov-64000
      (ce je ta pozitiven, vzames kar rezultat za naslov, ce negativen, je znotraj ranga)
      - ta nacin je hitrejsi kot pa ce delamo modulo.
      

 */

#define ZASLON 0xA000
/* stevilo pikslov - prvi nedosegljivi naslov, ce dostopamo linearno */
#define ZASLON_MAX 0xFA00
#define ZASLON_ZADNJA_VRSTICA 0xF8C0
/* prvi naslov zadnje vrstice v 16 bitnem prostoru - dobimo pri odstevanju v negativno vrednost */
#define ZASLON_NEGATIVNA_VRSTICA 0xFEC0

/* bodisi smo v 1. ciklu (0) ali pa v 2. ciklu (1) - glede na tega tudi nastavimo masko pri branju/pisanju */
char  cikel;

/* potrebno je paziti, kako delujeta funkciji za branje/pisanje glede na cikel
   1. cikel (cikel=0) - trenutni 2. bit, prejsnji 1. bit
   2. cikel (cikel=1) - trenutni 1. bit, prejsnji 2. bit
   
   beri vedno bere prejsnje stanje, pisi vedno pise trenutno stanje!
   
   beri:
   1. cikel: stanje = brano & 0x1
   2. cikel: stanje = brano >> 0x1
   poenostavimo v: stanje = (brano >> cikel)&1
   
   pisi
   1. cikel: brano & 0x1 | stanje << 1
   2. cikel: brano & 0x2 | stanje
   poenostavimo v: (brano & (1<<cikel)) | (stanje<<(1-cikel))
*/
/* svetla stran bcc-ja je po mojem mnenju ravno njegov listing
   (za ta program pridobljen kot bcc -Md -v conway.c -S), ki nam pred vsakim
   blokom v zbirniku nastavi labele za odmik bp do lokalnih vrednosti ter argumentov
   na skladu
   
   te on ustvari kot
      .ime_funkcije.ime_spremenljivke set ODMIK_NA_SKLADU
   Vendar treba je upostevati, da te on ustvari LE ce za dano funkcijo naredi tudi vstopno/izstopno
   tocko - zato je potrebno v funkcijah, ki nimajo na zacetku bloka nobenega C stavka,
   vriniti ;
   
   V vsakem primeru (ne glede na to ali ima funkcija vstopno/izstopno tocko) pa on
   ustvari kaksen je odmik argumentov od sp, kakrsen je bil po klicu call
   (torej kakrsen je takoj na zacetku funkcije), in sicer kot
      _ime_funkcije.ime_spremenljivke
*/
/* gremo se drzati sledecega pravila - za labele lokalnega konteksta, ki jih
   ustvari prevajalnik sam (npr. odmik lokalnih spr. na skladu) se vedno
   uporablja
   .ime_funkcije.ime_labele
   
   Ce pa gre za labele lokalnega konteksta, ki smo jih ustvarili sami, pa bodo
   brez pike na zacetku, torej bodo
   ime_funkcije.ime_labele
   
   Da bo lazje za razlikovati med njimi.
*/

unsigned char beri(idx)
unsigned int idx; /* na 4[bp] */
{
   ; /* ; NUJNO potreben, zato da bcc ustvari vstopno/izstopno tocko, pri cemer nastavi tudi bp */
#asm
   /* ax tu ne povrnemo - funkcije vracajo svoj rezultat v ax registru */
   push  bx
   push  cx
   push  ds

   mov   ax, #ZASLON
   mov   ds, ax

   xor   ax, ax
   mov   bx, .beri.idx[bp]
   mov   al, [bx] /* preberemo vrednost (KI JE 8-BITNA, 1 BAJT), shiftamo desno za cikel ter jo pustimo v ax - to je rezultat funkcije */
   
   pop   ds       /* povrnemo ds - potrebujemo, da preberemo globalno spremenljivko */
   mov   cl, [_cikel] /* v primeru 2. cikla shiftamo desno, da imamo vrednost na 1. bitu, hkrati pa izlocimo prejsnje stanje */
   shr   al, cl
   
   and   al, #0x1 /* naredimo bitni and na 1. bitu - s tem vzamemo le trenutni bit (v primeru 1. cikla se 2. bit posledicno zanemari) */
   
   pop   cx
   pop   bx
#endasm
   /* rezultat se vrne na ax */
}
void pisi(idx, vrednost)
unsigned int idx;
unsigned char vrednost;
{
   ; /* da bcc ustvari vstopno/izstopno tocko */
#asm
   push  cx
   mov   cx, [_cikel] /* predno zamenjamo ds, nalozimo se cikel */

   push  ds
   push  ax
   push  bx
   
   mov   ax, #ZASLON
   mov   ds, ax
   
   xor   ax, ax
   /* moramo delati read, modify, write */
   mov   bx, .pisi.idx[bp]
   mov   al, [bx] /* preberemo stanje celice */
   
   mov   ah, #1
   shl   ah, cl
   and   al, ah /* ohranimo prejsnji bit, trenutnega izlocimo - (brano & (1<<cikel)) */
   
   mov   ah, #1
   sub   ah, cl
   mov   cl, ah
   mov   ah, .pisi.vrednost[bp]
   shl   ah, cl /* preberemo novo trenutno stanje ter ga premaknemo - (stanje<<(1-cikel)) */
   
   or    al, ah /* zdruzimo prejsnje ter novo trenutno stanje */
   
   mov   [bx], al /* novo stanje zapisemo nazaj v pomnilnik */
   
   pop   bx
   pop   ax
   pop   ds
   
   pop   cx
#endasm
}

/* izracuna pravi naslov

   se en zanimiv hrosc bcc C-ja - ce je najvisji bit 16-bitne vrednosti 1, bo prevajalnik odstevanje
   (kljub temu da imas unsigned!) pretvoril v sestevanje z dvojiskim komplementom, pri
   cemer namesto 16-bitne zgenerira 64-bitno vrednost
      se vedno se da priti mimo tega, da namesto odstevanja naredis sestevanje z nasprotno vrednostjo tega
      - zaradi nekega razloga to prav interpretira
*/

unsigned int naslov(idx)
unsigned int idx;
{
   /* brez vstopne/izstopne tocke - v tem primeru dostopamo do argumentov na
      skladu kot odmik od TRENUTNE vrednosti stack pointerja! */
#asm
   /* ax ne ohranimo - pridobimo sp ter nalozimo argument iz njega */
   mov   ax, sp
   push  bx
   mov   bx, ax
   mov   ax, _naslov.idx[bx]
   pop   bx
   cmp   ax, #ZASLON_NEGATIVNA_VRSTICA
   jb    naslov.nad_min /* moramo izvajati unsigned primerjave! */
   /* naslov pod minimumom */
   add   ax, #ZASLON_MAX
   ret
naslov.nad_min:
   cmp   ax, #ZASLON_MAX
   jb    naslov.veljaven
   /* naslov nad minimumom */
   sub   ax, #ZASLON_MAX
   ret
naslov.veljaven:
   /* naslov v veljavnem obmocju, ne spreminjamo ga */
   ret
#endasm
}

/* nastavi paleto glede na trenutni cikel */
void nastavi_paleto()
{
/* Nastavimo paleto, pri cemer je indeks
 *  0 na (0,0,0), crna
 *  1 na (255,255,255) ce je 1. cikel (cikel == 0), cikel 2 pri (0,0,0)
 *  2 obratno od 1
 *  3 na (1,1,1)
 */
#asm
   push  ax
   push  cx
   push  dx
   
   xor   ax, ax
   mov   dx, #0x03C8
   out   dx, al
   inc   dx
   
   /* nastavljanje indeksov palete */
   /* 0 */
   out   dx, al /* nalozimo RGB [0] */
   out   dx, al
   out   dx, al
   
   /* 1 */
   /* ce je cikel ==0, gre za preklop iz prvega v drugi cikel,
    * 01 (pisi nastavi v cikel!=0) se skrije, 10 (pisi nastavi v cikel==0) prikaze */
   
   /* ce je cikel !=0, gre za preklop iz drugega v prvi cikel,
    * 10 (ki ga pisi nastavi ko cikel==0, sedaj trenutni cikel) se skrije, 01
    * (ki ga pisi nastavi ko cikel!=0, sedaj prejsnji cikel) prikaze */
   /* naredimo kot 0xFF<<(cikel<<3) */
   mov   al, [_cikel]
   mov   cl, #3
   shl   ax, cl
   mov   cl, al /* pridobili odmik, zdaj pa se tem odmikom premaknemo 0xFF */
   
   mov   ax, #0x00FF
   shl   ax, cl
   
   /* sedaj obravnavamo ah ter al posebej:
         al vsebuje RGB vrednost [1] indeksa,
         ah vsebuje RGB vrednost [2] indeksa */
   out   dx, al /* nalozimo RGB [1] */
   out   dx, al
   out   dx, al
   
   mov   al, ah
   out   dx, al /* nalozimo RGB [2] */
   out   dx, al
   out   dx, al
   /* 3 */
   mov   al, #0xFF
   out   dx, al /* nalozimo RGB [3] */
   out   dx, al
   out   dx, al
   
   pop   dx
   pop   cx
   pop   ax
#endasm
}

void pocakaj()
{
#asm
   /* klicemo bios wait interrupt, da cakamo, recimo 0.5s
      ceprav po pravici povedano bo program pomoje dovolj pocasen, da se ga bo preprosto
      videlo brez cakanja */
   push  ax
   push  cx
   push  dx
   
   mov   ah, #0x86
   mov   cx, #5
   mov   dx, #0
   
   int   #0x15
   
   pop dx
   pop cx
   pop ax
#endasm
}

/* =========================== ZACETNA POSTAVITEV =========================== */
/* Jaz bi sicer z najvecjim veseljem razvil se nalaganje stanja iz datoteke,
   vendar me mocno preganjajo se ostali projekti, tako da se bo treba
   zadovoljiti s tem */
unsigned int zac_vrstica = 98;
unsigned int zac_stolpec = 158;

unsigned int sirina = 5;
unsigned int visina = 5;

unsigned int zac_stanje[5][5] =
   {{1, 1, 1, 0, 1},
    {1, 0, 0, 0, 0},
    {0, 0, 0, 1, 1},
    {0, 1, 1, 0, 1},
    {1, 0, 1, 0, 1}};
//    {{0, 0, 0, 0, 0}, // glider, za probo
//     {0, 0, 1, 0, 0},
//     {0, 0, 0, 1, 0},
//     {0, 1, 1, 1, 0},
//     {0, 0, 0, 0, 0}};

/* ========================================================================== */

int main()
{
/* vsi cari K&R C - spremenljivke so deklarirane za deklaracijo funkcije, a pred blokom */

/* koordinate za nalagalnik zacetnega stanja */
unsigned int t_stolpec;
unsigned int t_vrstica;

/* indeks trenutno gledane celice */
unsigned int   celica_idx;
/* stevilo zivih celic */
unsigned char  st_zivih;
/* stanje trenutne celice */
unsigned char  t_celica;

// unsigned int i; /* za namen testiranja */

/* inicializacija graficnega nacina */
#asm
   push ax
   mov ah,     #0x00
   mov al,     #0x13
   int         #0x10
   pop ax
#endasm

   /* zaslon naj bi ze ob preklopu bil nastavljen na 0 */
   cikel=0; /* naredimo preklop iz 1 v 0, 0 bo viden */
   
   nastavi_paleto();
   
   /* nastavimo zacetno stanje */
   for(t_vrstica=0; t_vrstica<visina; t_vrstica++)
      for(t_stolpec=0; t_stolpec<sirina; t_stolpec++)
         pisi((t_vrstica+zac_vrstica)*320+(t_stolpec+zac_stolpec), zac_stanje[t_vrstica][t_stolpec]);
   cikel=1;
   nastavi_paleto();
   
while(1)
{
   /* SIMULACIJA */
   for(celica_idx=0; celica_idx<ZASLON_MAX; celica_idx+=1)
   {
      st_zivih = beri(naslov(celica_idx-321)) + beri(naslov(celica_idx-320)) + beri(naslov(celica_idx-319)) +
                 beri(naslov(celica_idx-1))   + /*    trenutna celica     */   beri(naslov(celica_idx+1)) +
                 beri(naslov(celica_idx+319)) + beri(naslov(celica_idx+320)) + beri(naslov(celica_idx+321));
      if(st_zivih<2 || st_zivih>3)
         pisi(celica_idx, 0); /* celica z manj kot dvema zivima sosedoma ali vec kot tremi umre */
      else
      {
         t_celica = beri(celica_idx);
         if(t_celica==0 && st_zivih==3) /* mrtva celica z natanko tremi zivimi sosedi ozivi */
            pisi(celica_idx, 1);
         else
            pisi(celica_idx, t_celica); /* ce je stevilo sosednjih celic 2 ali 3 ter ce je
                                           trenutna celica ze ziva, prezivi */
      }
   }
   /* CASOVNA ZAKASNITEV */
   pocakaj();
   /* MENJAVA CIKLA */
   cikel = (!cikel)&0x1;
   /* MENJAVA PALETE */
   nastavi_paleto();
}

/* vrnemo se v DOS */
/*
#asm
   mov ah,     #0x4C
   mov al,     #0x00
   int         #0x21
#endasm
*/
}

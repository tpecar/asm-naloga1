! 1 
! 1 # 1 "func_test.c"
! 1 
! 2 
! 3 int sum(a, b)
! 4 int a, b;
export	_sum
_sum:
!BCC_EOS
! 5 {
! 6     int c;
!BCC_EOS
! 7 #asm
push	bp
mov	bp,sp
push	di
push	si
dec	sp
dec	sp
!BCC_EOS
!BCC_ASM
_sum.b	set	$C
.sum.b	set	6
_sum.a	set	$A
.sum.a	set	4
_sum.c	set	0
.sum.c	set	-6
    push ax
    pop ax
! 10 endasm
!BCC_ENDASM
!BCC_EOS
! 11     c= a+b;
! Debug: add int b = [S+8+4] to int a = [S+8+2] (used reg = )
mov	ax,4[bp]
add	ax,6[bp]
! Debug: eq int = ax+0 to int c = [S+8-8] (used reg = )
mov	-6[bp],ax
!BCC_EOS
! 12     return c;
mov	ax,-6[bp]
inc	sp
inc	sp
pop	si
pop	di
pop	bp
ret
!BCC_EOS
! 13 }
! 14 
! 15 int main()
! 16 {
export	_main
_main:
! 17     int a=0, b=1;
push	bp
mov	bp,sp
push	di
push	si
dec	sp
dec	sp
! Debug: eq int = const 0 to int a = [S+8-8] (used reg = )
xor	ax,ax
mov	-6[bp],ax
dec	sp
dec	sp
! Debug: eq int = const 1 to int b = [S+$A-$A] (used reg = )
mov	ax,*1
mov	-8[bp],ax
!BCC_EOS
! 18     sum(a,b);
! Debug: list int b = [S+$A-$A] (used reg = )
push	-8[bp]
! Debug: list int a = [S+$C-8] (used reg = )
push	-6[bp]
! Debug: func () int = sum+0 (used reg = )
call	_sum
add	sp,*4
!BCC_EOS
! 19     return 0;
xor	ax,ax
add	sp,*4
pop	si
pop	di
pop	bp
ret
!BCC_EOS
! 20 }
! 21 
.data
.bss

! 0 errors detected

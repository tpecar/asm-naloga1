bits 16
section .txt
start:
    org  100h
    mov ah, 00h
    mov al, 03h
    int 10h
    ; preseravanje s crkami
    mov si, sporocilo
    call izp_niz

    mov ah, 4ch
    mov al, 00h
    int  21h
; ------------------------------- izp_niz ---------------------------------
; izpisemo string, ki je zakljucen z 0, woo
; v bx dobimo naslov na niz
; ------------------------------- izp_niz ---------------------------------
izp_niz:
    ; shranimo vrednosti registrov, ki jih umazemo
    push ax
    push si

    ; damo ah na Ah, ker bomo veckrat prozili isti interrupt za to
    mov ah, 0Eh
    mov cx, 02d

    izp_niz_isci:
    mov al,[si]
    or al,al    ; preverimo ce smo prisli ze do null znaka
    jz izp_niz_konec ; dosegli null
    ; ce se nismo dosegli null, izpisemo trenutni znak ter gremo iskati naprej

    int 10h

    add si,01d
    jmp izp_niz_isci

    izp_niz_konec:

    ; se vrnemo nazaj v glavni program
    pop si
    pop ax
    ret
; -------------------------------------------------------------------------
section .data
sporocilo  db  'Dobrodosel na planetu Zemlja!',0

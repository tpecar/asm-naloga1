! 1 
! 1 # 1 "conway.c"
! 1 # 131
! 131 char  cikel;
!BCC_EOS
! 132 # 177
! 177 unsigned char beri(idx)
! 178 unsigned int idx; 
export	_beri
_beri:
!BCC_EOS
! 179 {
! 180    ; 
push	bp
mov	bp,sp
push	di
push	si
!BCC_EOS
! 181 #asm
!BCC_EOS
!BCC_ASM
_beri.idx	set	8
.beri.idx	set	4
   
   push  bx
   push  cx
   push  ds

   mov   ax, #0xA000 
   mov   ds, ax

   xor   ax, ax
   mov   bx, .beri.idx[bp]
   mov   al, [bx] 
   
   pop   ds       
   mov   cl, [_cikel] 
   shr   al, cl
   
   and   al, #0x1 
   
   pop   cx
   pop   bx
! 202 endasm
!BCC_ENDASM
!BCC_EOS
! 203    
! 204 }
pop	si
pop	di
pop	bp
ret
! 205 void pisi(idx, vrednost)
! 206 unsigned int idx;
export	_pisi
_pisi:
!BCC_EOS
! 207 unsigned char vrednost;
!BCC_EOS
! 208 {
! 209    ; 
push	bp
mov	bp,sp
push	di
push	si
!BCC_EOS
! 210 #asm
!BCC_EOS
!BCC_ASM
_pisi.vrednost	set	$A
.pisi.vrednost	set	6
_pisi.idx	set	8
.pisi.idx	set	4
   push  cx
   mov   cx, [_cikel] 

   push  ds
   push  ax
   push  bx
   
   mov   ax, #0xA000 
   mov   ds, ax
   
   xor   ax, ax
   
   mov   bx, .pisi.idx[bp]
   mov   al, [bx] 
   
   mov   ah, #1
   shl   ah, cl
   and   al, ah 
   
   mov   ah, #1
   sub   ah, cl
   mov   cl, ah
   mov   ah, .pisi.vrednost[bp]
   shl   ah, cl 
   
   or    al, ah 
   
   mov   [bx], al 
   
   pop   bx
   pop   ax
   pop   ds
   
   pop   cx
! 245 endasm
!BCC_ENDASM
!BCC_EOS
! 246 }
pop	si
pop	di
pop	bp
ret
! 247 # 257
! 257 unsigned int naslov(idx)
! 258 unsigned int idx;
export	_naslov
_naslov:
!BCC_EOS
! 259 {
! 260 
! 261    
! 262 #asm
!BCC_ASM
_naslov.idx	set	2
   
   mov   ax, sp
   push  bx
   mov   bx, ax
   mov   ax, _naslov.idx[bx]
   pop   bx
   cmp   ax, #0xFEC0 
   jb    naslov.nad_min 
   
   add   ax, #0xFA00 
   ret
naslov.nad_min:
   cmp   ax, #0xFA00 
   jb    naslov.veljaven
   
   sub   ax, #0xFA00 
   ret
naslov.veljaven:
   
   ret
! 283 endasm
!BCC_ENDASM
! 284 }
ret
! 285 
! 286 
! 287 void nastavi_paleto()
! 288 {
export	_nastavi_paleto
_nastavi_paleto:
! 289 # 295
! 295 #asm
!BCC_ASM
   push  ax
   push  cx
   push  dx
   
   xor   ax, ax
   mov   dx, #0x03C8
   out   dx, al
   inc   dx
   
   
   
   out   dx, al 
   out   dx, al
   out   dx, al
   
   

   
   


   
   
   mov   al, [_cikel]
   mov   cl, #3
   shl   ax, cl
   mov   cl, al 
   
   mov   ax, #0x00FF
   shl   ax, cl
   


   
   out   dx, al 
   out   dx, al
   out   dx, al
   
   mov   al, ah
   out   dx, al 
   out   dx, al
   out   dx, al
   
   mov   al, #0xFF
   out   dx, al 
   out   dx, al
   out   dx, al
   
   pop   dx
   pop   cx
   pop   ax
! 347 endasm
!BCC_ENDASM
! 348 }
ret
! 349 
! 350 void pocakaj()
! 351 {
export	_pocakaj
_pocakaj:
! 352 #asm
!BCC_ASM


   
   push  ax
   push  cx
   push  dx
   
   mov   ah, #0x86
   mov   cx, #5
   mov   dx, #0
   
   int   #0x15
   
   pop dx
   pop cx
   pop ax
! 369 endasm
!BCC_ENDASM
! 370 }
ret
! 371 # 376
! 376 unsigned i
! 376 nt zac_vrstica = 98;
.data
export	_zac_vrstica
_zac_vrstica:
.word	$62
!BCC_EOS
! 377 unsigned int zac_stolpec = 158;
export	_zac_stolpec
_zac_stolpec:
.word	$9E
!BCC_EOS
! 378 
! 379 unsigned int sirina = 5;
export	_sirina
_sirina:
.word	5
!BCC_EOS
! 380 unsigned int visina = 5;
export	_visina
_visina:
.word	5
!BCC_EOS
! 381 
! 382 unsigned int zac_stanje[5][5] =
! 383    {{1, 1, 1, 0, 1},
export	_zac_stanje
_zac_stanje:
.word	1
.word	1
.word	1
.word	0
.word	1
! 384     {1, 0, 0, 0, 0},
.word	1
.word	0
.word	0
.word	0
.word	0
! 385     {0, 0, 0, 1, 1},
.word	0
.word	0
.word	0
.word	1
.word	1
! 386     {0, 1, 1, 0, 1},
.word	0
.word	1
.word	1
.word	0
.word	1
! 387     {1, 0, 1, 0, 1}};
.word	1
.word	0
.word	1
.word	0
.word	1
!BCC_EOS
! 388 # 396
! 396 int main()
! 397 {
.text
export	_main
_main:
! 398 
! 399 
! 400 
! 401 unsigned int t_stolpec;
!BCC_EOS
! 402 unsigned int t_vrstica;
!BCC_EOS
! 403 
! 404 
! 405 unsigned int   celica_idx;
!BCC_EOS
! 406 
! 407 unsigned char  st_zivih;
!BCC_EOS
! 408 
! 409 unsigned char  t_celica;
!BCC_EOS
! 410 # 414
! 414 #asm
push	bp
mov	bp,sp
push	di
push	si
add	sp,*-8
!BCC_EOS
!BCC_ASM
_main.t_celica	set	0
.main.t_celica	set	-$C
_main.t_stolpec	set	6
.main.t_stolpec	set	-6
_main.st_zivih	set	1
.main.st_zivih	set	-$B
_main.t_vrstica	set	4
.main.t_vrstica	set	-8
_main.celica_idx	set	2
.main.celica_idx	set	-$A
   push ax
   mov ah,     #0x00
   mov al,     #0x13
   int         #0x10
   pop ax
! 420 endasm
!BCC_ENDASM
!BCC_EOS
! 421 
! 422    
! 423    cikel=0; 
! Debug: eq int = const 0 to char = [cikel+0] (used reg = )
xor	al,al
mov	[_cikel],al
!BCC_EOS
! 424    
! 425    nastavi_paleto();
! Debug: func () void = nastavi_paleto+0 (used reg = )
call	_nastavi_paleto
!BCC_EOS
! 426    
! 427    
! 428    for(t_vrstica=0; t_vrstica<visina; t_vrstica++)
! Debug: eq int = const 0 to unsigned int t_vrstica = [S+$E-$A] (used reg = )
xor	ax,ax
mov	-8[bp],ax
!BCC_EOS
!BCC_EOS
! 429       for(t_stolpec=0; t_stolpec<sirina; t_stolpec++)
jmp .3
.4:
! Debug: eq int = const 0 to unsigned int t_stolpec = [S+$E-8] (used reg = )
xor	ax,ax
mov	-6[bp],ax
!BCC_EOS
!BCC_EOS
! 430          pisi((t_vrstica+zac_vrstica)*320+(t_stolpec+zac_stolpec), zac_stanje[t_vrstica][t_stolpec]);
jmp .7
.8:
! Debug: ptradd unsigned int t_vrstica = [S+$E-$A] to [5] [5] unsigned int = zac_stanje+0 (used reg = )
mov	bx,-8[bp]
mov	dx,bx
shl	bx,*1
shl	bx,*1
add	bx,dx
shl	bx,*1
! Debug: ptradd unsigned int t_stolpec = [S+$E-8] to [5] unsigned int = bx+_zac_stanje+0 (used reg = )
mov	ax,-6[bp]
shl	ax,*1
add	bx,ax
! Debug: list unsigned int = [bx+_zac_stanje+0] (used reg = )
push	_zac_stanje[bx]
! Debug: add unsigned int = [zac_stolpec+0] to unsigned int t_stolpec = [S+$10-8] (used reg = )
mov	ax,-6[bp]
add	ax,[_zac_stolpec]
push	ax
! Debug: add unsigned int = [zac_vrstica+0] to unsigned int t_vrstica = [S+$12-$A] (used reg = )
mov	ax,-8[bp]
add	ax,[_zac_vrstica]
! Debug: mul int = const $140 to unsigned int = ax+0 (used reg = )
mov	cx,#$140
imul	cx
! Debug: add unsigned int (temp) = [S+$12-$12] to unsigned int = ax+0 (used reg = )
add	ax,-$10[bp]
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () void = pisi+0 (used reg = )
call	_pisi
add	sp,*4
!BCC_EOS
! 431    cikel=1;
.6:
! Debug: postinc unsigned int t_stolpec = [S+$E-8] (used reg = )
mov	ax,-6[bp]
inc	ax
mov	-6[bp],ax
.7:
! Debug: lt unsigned int = [sirina+0] to unsigned int t_stolpec = [S+$E-8] (used reg = )
mov	ax,-6[bp]
cmp	ax,[_sirina]
jb 	.8
.9:
.5:
.2:
! Debug: postinc unsigned int t_vrstica = [S+$E-$A] (used reg = )
mov	ax,-8[bp]
inc	ax
mov	-8[bp],ax
.3:
! Debug: lt unsigned int = [visina+0] to unsigned int t_vrstica = [S+$E-$A] (used reg = )
mov	ax,-8[bp]
cmp	ax,[_visina]
jb 	.4
.A:
.1:
! Debug: eq int = const 1 to char = [cikel+0] (used reg = )
mov	al,*1
mov	[_cikel],al
!BCC_EOS
! 432    nastavi_paleto();
! Debug: func () void = nastavi_paleto+0 (used reg = )
call	_nastavi_paleto
!BCC_EOS
! 433    
! 434 while(1)
! 435 {
.D:
! 436    
! 437    for(celica_idx=0; celica_idx<0xFA00 ; celica_idx+=1)
! Debug: eq int = const 0 to unsigned int celica_idx = [S+$E-$C] (used reg = )
xor	ax,ax
mov	-$A[bp],ax
!BCC_EOS
!BCC_EOS
! 438    {
br 	.10
.11:
! 439       st_zivih = beri(naslov(celica_idx-321)) + beri(naslov(celica_idx-320)) + beri(naslov(celica_idx-319)) +
! 440                  beri(naslov(celica_idx-1))   +    beri(naslov(celica_idx+1)) +
! 441                  beri(naslov(celica_idx+319)) + beri(naslov(celica_idx+320)) + beri(naslov(celica_idx+321));
! Debug: add int = const $141 to unsigned int celica_idx = [S+$E-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax+$141 (used reg = )
add	ax,#$141
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: add int = const $140 to unsigned int celica_idx = [S+$10-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax+$140 (used reg = )
add	ax,#$140
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: add int = const $13F to unsigned int celica_idx = [S+$12-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax+$13F (used reg = )
add	ax,#$13F
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: add int = const 1 to unsigned int celica_idx = [S+$14-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax+1 (used reg = )
inc	ax
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: sub int = const 1 to unsigned int celica_idx = [S+$16-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax-1 (used reg = )
dec	ax
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: sub int = const $13F to unsigned int celica_idx = [S+$18-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax-$13F (used reg = )
add	ax,#-$13F
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: sub int = const $140 to unsigned int celica_idx = [S+$1A-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax-$140 (used reg = )
add	ax,#-$140
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
push	ax
! Debug: sub int = const $141 to unsigned int celica_idx = [S+$1C-$C] (used reg = )
mov	ax,-$A[bp]
! Debug: list unsigned int = ax-$141 (used reg = )
add	ax,#-$141
push	ax
! Debug: func () unsigned int = naslov+0 (used reg = )
call	_naslov
inc	sp
inc	sp
! Debug: list unsigned int = ax+0 (used reg = )
push	ax
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$1C-$1C] to unsigned char = al+0 (used reg = )
xor	ah,ah
add	al,-$1A[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$1A-$1A] to unsigned int = ax+0 (used reg = )
add	al,-$18[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$18-$18] to unsigned int = ax+0 (used reg = )
add	al,-$16[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$16-$16] to unsigned int = ax+0 (used reg = )
add	al,-$14[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$14-$14] to unsigned int = ax+0 (used reg = )
add	al,-$12[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$12-$12] to unsigned int = ax+0 (used reg = )
add	al,-$10[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: add unsigned char (temp) = [S+$10-$10] to unsigned int = ax+0 (used reg = )
add	al,-$E[bp]
adc	ah,*0
inc	sp
inc	sp
! Debug: eq unsigned int = ax+0 to unsigned char st_zivih = [S+$E-$D] (used reg = )
mov	-$B[bp],al
!BCC_EOS
! 442       if(st_zivih<2 || st_zivih>3)
! Debug: lt int = const 2 to unsigned char st_zivih = [S+$E-$D] (used reg = )
mov	al,-$B[bp]
cmp	al,*2
jb  	.13
.14:
! Debug: gt int = const 3 to unsigned char st_zivih = [S+$E-$D] (used reg = )
mov	al,-$B[bp]
cmp	al,*3
jbe 	.12
.13:
! 443          pisi(celica_idx, 0); 
! Debug: list int = const 0 (used reg = )
xor	ax,ax
push	ax
! Debug: list unsigned int celica_idx = [S+$10-$C] (used reg = )
push	-$A[bp]
! Debug: func () void = pisi+0 (used reg = )
call	_pisi
add	sp,*4
!BCC_EOS
! 444       else
! 445       {
jmp .15
.12:
! 446          t_celica = beri(celica_idx);
! Debug: list unsigned int celica_idx = [S+$E-$C] (used reg = )
push	-$A[bp]
! Debug: func () unsigned char = beri+0 (used reg = )
call	_beri
inc	sp
inc	sp
! Debug: eq unsigned char = al+0 to unsigned char t_celica = [S+$E-$E] (used reg = )
mov	-$C[bp],al
!BCC_EOS
! 447          if(t_celica==0 && st_zivih==3) 
! Debug: logeq int = const 0 to unsigned char t_celica = [S+$E-$E] (used reg = )
mov	al,-$C[bp]
test	al,al
jne 	.16
.18:
! Debug: logeq int = const 3 to unsigned char st_zivih = [S+$E-$D] (used reg = )
mov	al,-$B[bp]
cmp	al,*3
jne 	.16
.17:
! 448             pisi(celica_idx, 1);
! Debug: list int = const 1 (used reg = )
mov	ax,*1
push	ax
! Debug: list unsigned int celica_idx = [S+$10-$C] (used reg = )
push	-$A[bp]
! Debug: func () void = pisi+0 (used reg = )
call	_pisi
add	sp,*4
!BCC_EOS
! 449          else
! 450             pisi(celica_idx, t_celica);
jmp .19
.16:
! Debug: list unsigned char t_celica = [S+$E-$E] (used reg = )
mov	al,-$C[bp]
xor	ah,ah
push	ax
! Debug: list unsigned int celica_idx = [S+$10-$C] (used reg = )
push	-$A[bp]
! Debug: func () void = pisi+0 (used reg = )
call	_pisi
add	sp,*4
!BCC_EOS
! 451  
! 452       }
.19:
! 453    }
.15:
! 454    
! 455    pocakaj();
.F:
! Debug: addab int = const 1 to unsigned int celica_idx = [S+$E-$C] (used reg = )
mov	ax,-$A[bp]
inc	ax
mov	-$A[bp],ax
.10:
! Debug: lt unsigned int = const $FA00 to unsigned int celica_idx = [S+$E-$C] (used reg = )
mov	ax,-$A[bp]
cmp	ax,#$FA00
blo 	.11
.1A:
.E:
! Debug: func () void = pocakaj+0 (used reg = )
call	_pocakaj
!BCC_EOS
! 456    
! 457    cikel = (!cikel)&0x1;
mov	al,[_cikel]
test	al,al
jne 	.1B
.1C:
mov	al,*1
jmp	.1D
.1B:
xor	al,al
.1D:
! Debug: and int = const 1 to char = al+0 (used reg = )
and	al,*1
! Debug: eq char = al+0 to char = [cikel+0] (used reg = )
mov	[_cikel],al
!BCC_EOS
! 458    
! 459    nastavi_paleto();
! Debug: func () void = nastavi_paleto+0 (used reg = )
call	_nastavi_paleto
!BCC_EOS
! 460 }
! 461 # 470
! 470 }
.C:
br 	.D
.1E:
.B:
add	sp,*8
pop	si
pop	di
pop	bp
ret
! 471 
! Register BX used in function main
.data
.bss
.comm	_cikel,1

! 0 errors detected

/* da ugotovimo kako ta ljubi bcc sploh implementira funkcije */

int sum(a, b)
int a, b;
{
    /* INICIALIZACIJO LOKALNIH SPREMENLJIVK VEDNO NAREDI PRED PRVIM ASM BLOKOM KER DRUGACE TI ZA ASM BLOK NE NASTAVI BP */
    int c;
#asm
    push ax
    pop ax
#endasm
    c= a+b;
    return c;
}

int main()
{
    int a=0, b=1;
    sum(a,b);
    return 0;
}
